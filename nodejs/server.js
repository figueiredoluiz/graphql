var express = require('express');
var { graphqlHTTP } = require('express-graphql');
var { buildSchema } = require('graphql');
var sofa = require("sofa-api").default;

// Construct a schema
var schema = buildSchema(`
    type Query {
        hello: String
        posts: [Post]
        post(id: Int): Post
        authors: [Person]
        author(id: Int): Person
    }

    type Post {
        id: Int
        author: Person
        body: String
    }

    type Person {
        id: Int
        posts: [Post]
        firstName: String
        lastName: String
    }

    type Mutation {
        submitPost(input: PostInput!): Post
        createAuthor(input: AuthorInput!): Person
    }

    input PostInput {
        id: Int
        body: String!
        author: AuthorInput!
    }

    input AuthorInput {
        id: Int
        firstName: String
        lastName: String
    }
`);

// some data
const PEOPLE = new Map()
const POSTS = new Map()

class Post {
    constructor (data) { Object.assign(this, data) }
    get author () {
        return PEOPLE.get(this.authorId)
    }
}

class Person {
  constructor (data) { Object.assign(this, data) }
  get posts () {
    return [...POSTS.values()].filter(post => post.authorId === this.id)
  }
}

const initializeData = () => {
    const fakePeople = [
        { id: '1', firstName: 'John', lastName: 'Doe' },
        { id: '2', firstName: 'Jane', lastName: 'Doe' }
    ]
    fakePeople.forEach(person => PEOPLE.set(person.id, new Person(person)))

    const fakePosts = [
        { id: '1', authorId: '1', body: 'Hello world' },
        { id: '2', authorId: '2', body: 'Hi, planet!' }
    ]
    fakePosts.forEach(post => POSTS.set(post.id, new Post(post)))
}
initializeData()

// The root provides a resolver function for each API endpoint
var root = {
    hello: () => {
        return 'Hello world!';
    },
    posts: () => POSTS.values(),
    post: ({ id }) => POSTS.get(id),
    authors: () => PEOPLE.values(),
    author: ({ id }) => PEOPLE.get(id),
    
    submitPost: async ({ input }) => {
        let author = input.author.id;
        if (input.id!==undefined && POSTS.get(input.id)) {
            return null
        }

        if (!PEOPLE.has(author)) {
            let authorData = input.author
            PEOPLE.set(author, new Person(authorData))
        }

        var data = {
            id: POSTS.size+1,
            authorId: author,
            body: input.body
        }
        POSTS.set(data.id, new Post(data))
        return POSTS.get(data.id)
    },
    createAuthor: async ({ input }) => {
        if (PEOPLE.has(input.id)) return null
        input.id = PEOPLE.size+1
        PEOPLE.set(input.id, new Person(input))
        return PEOPLE.get(input.id)
    }
};

// server
var app = express();
app.use('/graphql', graphqlHTTP({
    schema: schema,
    rootValue: root,
    graphiql: true,
}));
app.use("/rest", sofa({
    schema: schema
}));
app.listen(4000);
console.log('Running a GraphQL API server at http://localhost:4000/graphql');
