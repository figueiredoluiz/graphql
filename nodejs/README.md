# 概要

簡単なGraphQL実装とRest API

# 使い方

①　`npm install`
②　`npm start`
②　ChromeでGraphQLクライアントかターミナルでCurlからリクエストをして下さい。
# GraphQL

## Query

```
query {
  posts {
    id
    author {
      id
      firstName
      lastName
    }
    body
  }
}
```

```
query {
  authors {
    id
    firstName
    lastName
  }
}
```

Curlで
curl -X POST -H "Content-Type: application/json" \
--data '{"query": "{ hello }" }' \
http://localhost:4000/graphql

## Mutation

```
mutation {
  createAuthor(input: {
    firstName: "Test"
    lastName: "Family"
  }) {
    id
    firstName
    lastName
  }
}
```

```
mutation {
  submitPost(input: {
    body: "Hello, world!"
    author: {
      id: 4
      firstName: "Joel"
      lastName: "Maria"
    }
  }) {
    id
    body
    author {
      id
      firstName
      lastName
    }
  }
}
```



# REST

rest endpoint  http://localhost:4000/rest