# 概要

簡単なGraphQL

# 使い方

①　`composer install`
②　`php -S localhost:8080`
②　ChromeでGraphQLクライアントかターミナルでCurlからリクエストをして下さい。

# GraphQL

## Query

```
query {
  echo(message: "Hello World")
}
```

Curlで
`curl http://localhost:8080 -d '{"query": "query { echo(message: \"Hello World\") }" }'`

## Mutation

```
mutation {
  sum(x: 2, y: 2)
}
```

Curlで
`curl http://localhost:8080 -d '{"query": "mutation { sum(x: 2, y: 2) }" }'`
